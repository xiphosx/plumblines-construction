# Plumblines Construction

This is a landing page for a local business in Richmond, Plumblines Construction. I was asked to update and redo the owner's website. 

I started with a mobile landing page first since most people are on mobile and it makes sense then to design there first and branch out. 

#SEO

I added a robot.txt file to disallow the login page. I also added a .htacces page for caching purposes. You'll find a local business schema in the header, too.

An example of what the page looks like is found here:
https://xiphosx.gitlab.io/plumblines-construction